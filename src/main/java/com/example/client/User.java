package com.example.client;

import java.util.Date;

public class User {

    private String name;
    private Date birthDate;
    private String phone;
    private String email;
    private Date modificationTime;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getModificationTime() {
        return modificationTime;
    }

    public void setModificationTime(Date modificationTime) {
        this.modificationTime = modificationTime;
    }

    public User(String name, Date birthDate, String phone, String email) {
        this.name = name;
        this.birthDate = birthDate;
        this.phone = phone;
        this.email = email;
        this.modificationTime = new Date();
    }

    public User() {
        this.name = "";
        this.birthDate = new Date();
        this.phone = "";
        this.email = "";
        this.modificationTime = new Date();
    }
}
