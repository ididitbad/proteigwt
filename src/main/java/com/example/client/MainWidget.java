package com.example.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.i18n.shared.DateTimeFormat;
import java.util.ArrayList;
import java.util.Date;

public class MainWidget extends Composite {

    interface MainWidgetUiBinder extends UiBinder<HTMLPanel, MainWidget> {
    }

    private static MainWidgetUiBinder ourUiBinder = GWT.create(MainWidgetUiBinder.class);

    @UiField
    VerticalPanel verticalPanel;
    @UiField
    DoubleClickFlexTable flexTable;
    @UiField
    Button createButton;

    @UiField
    DialogBox dialogBox;
    @UiField
    VerticalPanel dialogVerticalPanel;

    @UiField
    HorizontalPanel labelPanel;
    @UiField
    Label nameLabel;
    @UiField
    Label birthDateLabel;
    @UiField
    Label phoneLabel;
    @UiField
    Label emailLabel;

    @UiField
    HorizontalPanel textPanel;
    @UiField
    TextBox name;
    @UiField
    TextBox birthDate;
    @UiField
    TextBox phone;
    @UiField
    TextBox email;
    @UiField
    Button okButton;

    private ArrayList<User> users;

    int currUser = 0;

    public MainWidget() {
        initWidget(ourUiBinder.createAndBindUi(this));

        dialogBox.hide();
        dialogBox.setVisible(false);
        dialogBox.setText("Edit User Info");
        nameLabel.setText("Name");
        birthDateLabel.setText("BirthDate");
        phoneLabel.setText("Phone");
        emailLabel.setText("Email");
        okButton.setText("OK");

        String protei = "protei";
        users = new ArrayList<>();
        StringBuilder phoneBuilder = new StringBuilder();
        StringBuilder nameBuilder = new StringBuilder();
        for (int i = 0; i < protei.length(); i++) {
            phoneBuilder.setLength(0);
            nameBuilder.setLength(0);
            for (int j = 0; j < 11; j++) phoneBuilder.append(i + 1);
            for (int j = 0; j < 7; j++) nameBuilder.append(protei.charAt(i));
            users.add(new User(String.valueOf(protei.charAt(i)), new Date(i + 1,i + 1,i + 1),
                    "+" + phoneBuilder.toString(), nameBuilder.toString() + "@mail.com"));
        }

        drawTable();

        createButton.setText("Create new user");
        createButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
                User user = new User();
                users.add(user);
                currUser = users.indexOf(user);
                changeUser(currUser);
            }
        });

        flexTable.addDoubleClickHandler(new DoubleClickHandler() {
            @Override
            public void onDoubleClick(DoubleClickEvent doubleClickEvent) {
                HTMLTable.Cell clicked = flexTable.getCellForEvent(doubleClickEvent);
                if (clicked.getCellIndex() < 5) {
                    int row = clicked.getRowIndex();
                    currUser = row - 1;
                    changeUser(currUser);
                }
            }
        });

        okButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
//                User user = users.get(users.size() - 1);
                User user = users.get(currUser);
                user.setName(name.getText());
                DateTimeFormat fm = DateTimeFormat.getFormat("dd.MM.yyyy");
                user.setBirthDate(fm.parse(birthDate.getText()));
                user.setPhone(phone.getText());
                user.setEmail(email.getText());
                user.setModificationTime(new Date());
                flexTable.removeAllRows();
                drawTable();
                dialogBox.hide();
            }
        });
    }

    private void drawTable() {
        flexTable.setText(0,0, "Name");
        flexTable.setText(0,1, "Birth Date");
        flexTable.setText(0,2, "Phone");
        flexTable.setText(0,3, "Email");
        flexTable.setText(0,4, "Modification Time");

        for (User user: users) {
            addUser(user);
        }
    }

    private void addUser(User user) {
        int row = flexTable.getRowCount();
        flexTable.setText(row, 0, user.getName());
        DateTimeFormat fm = DateTimeFormat.getFormat("dd.MM.yyyy");
        flexTable.setText(row, 1, fm.format(user.getBirthDate()));
        flexTable.setText(row, 2, user.getPhone());
        flexTable.setText(row, 3, user.getEmail());
        fm = DateTimeFormat.getFormat("dd.MM.yyyy hh:mm:ss");
        flexTable.setText(row, 4, fm.format(user.getModificationTime()));

        Button removeButton = new Button("X");
        removeButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
                int rmIndex = users.indexOf(user);
                users.remove(rmIndex);
                flexTable.removeRow(rmIndex + 1);
            }
        });

        flexTable.setWidget(row, 5, removeButton);
    }

    private void changeUser(int curr) {
        User user = users.get(curr);
        name.setText(user.getName());
        DateTimeFormat fm = DateTimeFormat.getFormat("dd.MM.yyyy");
        birthDate.setText(fm.format(user.getBirthDate()));
        phone.setText(user.getPhone());
        email.setText(user.getEmail());
        dialogBox.center();
    }
}